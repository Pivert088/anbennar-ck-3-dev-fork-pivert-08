#k_bahar
##d_aqatbahar
###c_aqatbar
536 = {		#Aqatbar

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2891 = {

    # Misc
    holding = city_holding

    # History

}
2892 = {

    # Misc
    holding = church_holding

    # History

}
2893 = {

    # Misc
    holding = city_holding

    # History

}
2894 = {

    # Misc
    holding = none

    # History

}
2895 = {

    # Misc
    holding = none

    # History

}

###c_eduz_szel_ninezim
541 = {		#Eduz-szel-Ninezim

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2896 = {

    # Misc
    holding = city_holding

    # History

}
2897 = {

    # Misc
    holding = none

    # History

}

###c_nisabat
532 = {		#Nisabat

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2898 = {

    # Misc
    holding = church_holding

    # History

}
2899 = {

    # Misc
    holding = none

    # History

}

###c_jesdbar
544 = {		#Jesdbar

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2934 = {

    # Misc
    holding = church_holding

    # History

}

###c_arzdilsu
537 = {		#Arzdilsu

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2931 = {

    # Misc
    holding = church_holding

    # History

}
2932 = {

    # Misc
    holding = city_holding

    # History

}
2933 = {

    # Misc
    holding = none

    # History

}

##d_magairous
###c_vernat
530 = {		#Vernat

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2859 = {

    # Misc
    holding = city_holding

    # History

}

###c_eduz_wez
531 = {		#Eduz-Wez

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2860 = {

    # Misc
    holding = church_holding

    # History

}
2861 = {

    # Misc
    holding = none

    # History

}

###c_pir_ail
533 = {		#Pir-Ail

    # Misc
    culture = sun_elvish
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_agshelum
535 = {		#Agshelum

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2862 = {

    # Misc
    holding = city_holding

    # History

}
2863 = {

    # Misc
    holding = none

    # History

}

###c_bazibar
534 = {		#Bazibar

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2864 = {

    # Misc
    holding = none

    # History

}

##d_birsartenslib
###c_birsartansbar
549 = {		#Birsartansbar

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2865 = {

    # Misc
    holding = church_holding

    # History

}
2866 = {

    # Misc
    holding = none

    # History

}

###c_deshkumar
543 = {		#Deshkumar

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2867 = {

    # Misc
    holding = city_holding

    # History

}
2868 = {

    # Misc
    holding = church_holding

    # History

}

###c_fajabahar
542 = {		#Fajabahar

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2869 = {

    # Misc
    holding = city_holding

    # History

}
2870 = {

    # Misc
    holding = none

    # History

}

##d_bahar_szel_uak
###c_akal_uak
550 = {		#Akal Uak

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2871 = {

    # Misc
    holding = city_holding

    # History

}
2872 = {

    # Misc
    holding = church_holding

    # History

}
2873 = {

    # Misc
    holding = none

    # History

}
2874 = {

    # Misc
    holding = none

    # History

}

###c_tremo_enkost
547 = {		#Tremo'enkost

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2875 = {

    # Misc
    holding = church_holding

    # History

}
2876 = {

    # Misc
    holding = none

    # History

}
2877 = {

    # Misc
    holding = none

    # History

}

###c_habqez
548 = {		#Habqez

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2878 = {

    # Misc
    holding = city_holding

    # History

}
2879 = {

    # Misc
    holding = none

    # History

}

###c_forramaz
545 = {		#Forramaz

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2880 = {

    # Misc
    holding = none

    # History

}

##d_azka_evran
###c_azka_evran
538 = {		#Azka-Evran

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2881 = {

    # Misc
    holding = city_holding

    # History

}
2882 = {

    # Misc
    holding = church_holding

    # History

}
2883 = {

    # Misc
    holding = none

    # History

}
2884 = {

    # Misc
    holding = none

    # History

}

###c_qasnabor
539 = {		#Qasnabor

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2885 = {

    # Misc
    holding = none

    # History

}

###c_nasru_ean
540 = {		#Nasru-ean

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2886 = {

    # Misc
    holding = church_holding

    # History

}
2887 = {

    # Misc
    holding = none

    # History

}

##d_azkasad
###c_azkasad
650 = {		#Azkasad

    # Misc
    culture = gelkar
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2888 = {

    # Misc
    holding = church_holding

    # History

}
2889 = {

    # Misc
    holding = none

    # History

}

###c_daqelum
649 = {		#Daqelum

    # Misc
    culture = gelkar
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}
2890 = {

    # Misc
    holding = none

    # History

}